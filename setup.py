from distutils.core import setup

setup(name='tulip',
      version='0.1',
      description='Tulip project is an event-driven architecture demonstrator for Kitchen Hub',
      author='Bohdan Mushkevych',
      author_email='mushkevych@gmail.com',
      url='',
      packages=['tulip', 'tulip.model', 'tulip.system', 'tulip.workers', 'tulip.rest'],
      long_description='''Tulip is an event-driven architecture demonstrator for Kitchen Hub''',
      license='Apache 2.0',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Environment :: Web Environment',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: Apache Software License',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Utilities',
      ],
      install_requires=['werkzeug', 'jinja2', 'amqp', 'pymongo', 'psutil', 'fabric', 'setproctitle', 'synergy_odm',
                        'synergy_scheduler', 'mock', 'flask']
      )
