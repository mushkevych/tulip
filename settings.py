ENVIRONMENT = '%ENVIRONMENT%'

# folder locations, connection properties etc
settings = dict(
    process_prefix='Tulip',     # global prefix that is added to every process name started in this project
    process_cwd='/tmp',     # daemonized process working directory, where it can create .cache and other folders

    # mq_timeout_sec=300.0, # time in sec to wait for new message; otherwise terminate the worker; None to wait forever
    mq_queue='default_queue',
    mq_routing_key='default_routing_key',
    mq_exchange='default_exchange',
    mq_durable=True,
    mq_exclusive=False,
    mq_auto_delete=False,
    mq_delivery_mode=2,
    mq_no_ack=False,

    log_directory='/tmp/logs/tulip/',
    pid_directory='/tmp/logs/tulip/',

    perf_ticker_interval=60,    # seconds between performance ticker messages
    debug=False,                # if True - logger is given additional "console" adapter
    under_test=False,
)


# For now just two level... we can have configs for all deployments
# Need to have a better way for switching these
try:
    overrides = __import__('settings_' + ENVIRONMENT)
except:
    overrides = __import__('settings_dev')
settings.update(overrides.settings)


# Modules to test and verify (pylint/pep8)
testable_modules = [
    'model',
    'system',
    'workers',
]

test_cases = [
    'tests.test_scraper_engines',
    'tests.test_scrape_reducer',
]


def enable_test_mode():
    if settings['under_test']:
        # test mode is already enabled
        return

    test_settings = dict(
        mq_timeout_sec=10.0,
        mq_vhost='/unit_test',
        csv_bulk_threshold=64,
        debug=True,
        under_test=True,
    )
    settings.update(test_settings)

    from tests.ut_process_context import register_unit_test_context
    register_unit_test_context()
