__author__ = 'Bohdan Mushkevych'

from synergy.system.performance_tracker import TickerThread, TrackerPair


class ReceptionTracker(TickerThread):
    TRACKER_GROUP_REQUEST = 'request'

    def __init__(self, logger):
        super(ReceptionTracker, self).__init__(logger)
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_REQUEST))

    def __getattr__(self, item):
        if item in self.trackers:
            return self.trackers[item]


class ResupplyTracker(TickerThread):
    TRACKER_GROUP_REQUEST = 'request'
    TRACKER_GROUP_UPDATE = 'update'
    TRACKER_GROUP_DELEGATE = 'delegate'

    def __init__(self, logger):
        super(ResupplyTracker, self).__init__(logger)
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_REQUEST))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_UPDATE))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_DELEGATE))

    def __getattr__(self, item):
        if item in self.trackers:
            return self.trackers[item]


class KitchenTracker(TickerThread):
    TRACKER_GROUP_REQUEST = 'request'
    TRACKER_GROUP_RESUPPLY = 'resupply'
    TRACKER_GROUP_UPDATE = 'update'
    TRACKER_GROUP_ACCEPTED = 'accepted'
    TRACKER_GROUP_PENDING = 'pending'
    TRACKER_GROUP_COOKING = 'cooking'
    TRACKER_GROUP_COMPLETE = 'complete'
    TRACKER_GROUP_CANCELLED = 'cancelled'

    def __init__(self, logger):
        super(KitchenTracker, self).__init__(logger)
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_REQUEST))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_RESUPPLY))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_UPDATE))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_ACCEPTED))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_PENDING))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_COOKING))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_COMPLETE))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_CANCELLED))

    def __getattr__(self, item):
        if item in self.trackers:
            return self.trackers[item]


class DeliveryTracker(TickerThread):
    TRACKER_GROUP_REQUEST = 'request'
    TRACKER_GROUP_OUT = 'out'
    TRACKER_GROUP_IN_TRANSIT = 'in_transit'
    TRACKER_GROUP_DELIVERED = 'delivered'

    def __init__(self, logger):
        super(DeliveryTracker, self).__init__(logger)
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_REQUEST))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_OUT))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_IN_TRANSIT))
        self.add_tracker(TrackerPair(self.TRACKER_GROUP_DELIVERED))

    def __getattr__(self, item):
        if item in self.trackers:
            return self.trackers[item]
