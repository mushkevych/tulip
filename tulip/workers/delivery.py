__author__ = 'Bohdan Mushkevych'

import socket
from threading import Thread

from amqp import AMQPError
from synergy.mq.flopsy import PublishersPool, Consumer
from synergy.system.synergy_process import SynergyProcess

import settings
from constants import QUEUE_DELIVERY_REQUEST, QUEUE_DELIVERY_UPDATE
from tulip.model.food_order import FoodOrder
from tulip.model.food_order_state import FoodOrderState
from tulip.model.model_constants import *
from tulip.system.performance_tracker import DeliveryTracker


class Delivery(SynergyProcess):
    """ Module Delivery takes ownership of the FoodOrder after it leaves Kitchen and ensures it
        is delivered to the customer
    """

    def __init__(self, process_name):
        super(Delivery, self).__init__(process_name)
        self.publishers = PublishersPool(self.logger)
        self.mq_timeout_seconds = settings.settings.get('mq_timeout_sec')

        # MQ consumers
        self.request_consumer = None
        self.update_consumer = None

        # MQ threads
        self.request_thread = None
        self.update_thread = None

        # format: <food_order_id: FoodOrder>
        self.food_orders = dict()

        self._init_mq_consumer()
        self.performance_tracker = None
        self._init_performance_tracker(self.logger)
        self.logger.info(f'Started {self.process_name}')

    def __del__(self):
        try:
            self.logger.info('Closing Flopsy Publishers Pool...')
            self.publishers.close()
        except Exception as e:
            self.logger.error(f'Exception caught while closing Flopsy Publishers Pool: {e}')

        for consumer in [self.request_consumer, self.update_consumer]:
            try:
                self.logger.info('Closing Flopsy Consumer...')
                consumer.close()
            except Exception as e:
                self.logger.error(f'Exception caught while closing Flopsy Consumer: {e}')

        super(Delivery, self).__del__()

    def _init_performance_tracker(self, logger):
        # notice - we are not starting the thread. only creating an instance
        self.performance_tracker = DeliveryTracker(logger)
        self.performance_tracker.start()

    def _init_mq_consumer(self):
        self.request_consumer = Consumer(QUEUE_DELIVERY_REQUEST)
        self.update_consumer = Consumer(QUEUE_DELIVERY_UPDATE)

    def mq_callback_update(self, message):
        new_state = FoodOrderState.from_json(message.body)
        assert isinstance(new_state, FoodOrderState)
        if new_state.order_id not in self.food_orders:
            raise KeyError(f'order_id={new_state.order_id} is beyond the horizon. '
                           f'skipping the update operation')

        food_order = self.food_orders[new_state.order_id]
        self.performance_tracker.request.increment_success()

        if new_state.state == STATE_OUT_FOR_DELIVERY:
            food_order.order_state = new_state
            self.performance_tracker.out.increment_success()

        if new_state.state == STATE_IN_TRANSIT:
            food_order.order_state = new_state
            self.performance_tracker.in_transit.increment_success()

        if new_state.state == STATE_DELIVERED:
            # update DB record that this FoodOrder is complete
            # NOTE: DB portion is not implemented in the demonstrator

            # Food Order EOL
            del self.food_orders[new_state.order_id]
            self.performance_tracker.delivered.increment_success()

    def mq_callback_request(self, message):
        """ main thread MQ callback """
        try:
            food_order = FoodOrder.from_json(message.body)
            assert isinstance(food_order, FoodOrder)
            self.food_orders[food_order.order_id] = food_order

            self.request_consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.request.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.request_consumer.cancel(message.delivery_tag)
            self.performance_tracker.request.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.request_consumer.reject(message.delivery_tag)
            self.performance_tracker.request.increment_failure()

    def run_mq_listener(self, consumer, mq_callback):
        def runner():
            try:
                consumer.register(mq_callback)
                consumer.wait(self.mq_timeout_seconds)
            except socket.timeout as e:
                self.logger.warning(f'Queue {consumer.queue} is likely empty. Worker exits due to: {e}')
            except (AMQPError, IOError) as e:
                self.logger.error(f'AMQPError: {e}')
            finally:
                self.__del__()
                self.logger.info('Exiting main thread. All auxiliary threads stopped.')

        return runner

    def start(self, *_):
        self.request_thread = Thread(target=self.run_mq_listener(self.request_consumer, self.mq_callback_request))
        self.request_thread.start()

        self.update_thread = Thread(target=self.run_mq_listener(self.update_consumer, self.mq_callback_update))
        self.update_thread.start()


if __name__ == '__main__':
    from constants import PROCESS_DELIVERY

    worker = Delivery(PROCESS_DELIVERY)
    worker.start()
