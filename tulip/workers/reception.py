__author__ = 'Bohdan Mushkevych'

import threading

from synergy.mq.flopsy import PublishersPool, Consumer
from synergy.workers.abstract_mq_worker import AbstractMqWorker

from constants import QUEUE_KITCHEN_REQUEST, QUEUE_RECEPTION_REQUEST
from tulip.model.food_order import FoodOrder
from tulip.model.model_constants import *
from tulip.system.performance_tracker import ReceptionTracker


class Reception(AbstractMqWorker):
    """
    this class reads Food Orders from MQ and ensures that all necessary components are ready
    follow-up requests are then published to MQ for further processing
    """

    def __init__(self, process_name):
        super(Reception, self).__init__(process_name)
        self.publishers = PublishersPool(self.logger)
        self.initial_thread_count = threading.active_count()

    def __del__(self):
        try:
            self.logger.info('Closing Flopsy Publishers Pool...')
            self.publishers.close()
        except Exception as e:
            self.logger.error(f'Exception caught while closing Flopsy Publishers Pool: {e}')

        super(Reception, self).__del__()

    def _init_performance_tracker(self, logger):
        self.performance_tracker = ReceptionTracker(logger)
        self.performance_tracker.start()

    def _init_mq_consumer(self):
        self.consumer = Consumer(QUEUE_RECEPTION_REQUEST)

    def _mq_callback(self, message):
        # perform simple routing
        food_order = FoodOrder.from_json(message.body)
        if food_order.delivery_address.state.lower() != 'or' \
                and food_order.delivery_address.city.lower() != 'portland':
            self.performance_tracker.request.increment_failure()
            self.logger.error(f'Your city of {food_order.delivery_address} is currently not in the network')

            self.consumer.acknowledge(message.delivery_tag)
            return

        publisher = self.publishers.get(QUEUE_KITCHEN_REQUEST)
        try:
            # step 1: set state to CREATED
            food_order.order_state.state = STATE_CREATED

            # step 2: persist FoodOrder in a DB
            # NOTE: no DB in the demonstrator

            # step 3: publish updated document to the identified Kitchen
            publisher.publish(food_order.document)
            self.consumer.acknowledge(message.delivery_tag)

            # step 4: increment monitoring tracker
            self.performance_tracker.request.increment_success()

        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.consumer.cancel(message.delivery_tag)
            self.performance_tracker.request.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.consumer.reject(message.delivery_tag)
            self.performance_tracker.request.increment_failure()
        finally:
            publisher.release()


if __name__ == '__main__':
    from constants import PROCESS_PANTRY

    worker = Reception(PROCESS_PANTRY)
    worker.start()
