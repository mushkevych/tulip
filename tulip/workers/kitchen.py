__author__ = 'Bohdan Mushkevych'

import socket
from collections import defaultdict
from datetime import datetime, timedelta
from threading import Thread

from amqp import AMQPError
from synergy.mq.flopsy import PublishersPool, Consumer
from synergy.system.synergy_process import SynergyProcess

import settings
from constants import QUEUE_DELIVERY_REQUEST, QUEUE_PANTRY_REQUEST, QUEUE_KITCHEN_UPDATE, \
    QUEUE_KITCHEN_RESUPPLY, QUEUE_KITCHEN_REQUEST
from tulip.model.food_order import FoodOrder
from tulip.model.food_order_state import FoodOrderState
from tulip.model.model_constants import *
from tulip.model.resupply_update import ResupplyUpdate
from tulip.model.supply_request import SupplyRequest
from tulip.system.performance_tracker import KitchenTracker


class Kitchen(SynergyProcess):
    """ Kitchen:
        * receives Food Orders from the Reception
        * acknowledges the Food Order by moving its state to STATE_ACCEPTED  them if it has enough ingredients
        * sends request to Pantry if ingredients (groceries, packaging, cutlery, etc) are close to exhaustion
        * should some ingredient ran out, the menu is updated accordingly
        *

        Kitchen is a single-tenant process and is responsible for handling this one kitchen
        rationale behind this is following:
        * kitchen load will not be high in any given hub (ten requests per second, maybe?)
          that would allow to handle them by a single worker and maintain the order_state within
        * Kitchen daemon is responsible for raising alert if the Food Order is delayed
        * number of kitchens will also be very manageable
          for instance: let's say we have a Kitchen Hub per 25'000 people, and every Hub has 10 individual kitchens
          then for the whole US we will have 350'000'000 / 25'000 = 14'000 pods with 10-20 processes each
    """

    def __init__(self, process_name):
        super(Kitchen, self).__init__(process_name)
        self.publishers = PublishersPool(self.logger)
        self.mq_timeout_seconds = settings.settings.get('mq_timeout_sec')

        # MQ consumers
        self.request_consumer = None
        self.update_consumer = None
        self.resupply_consumer = None

        # MQ threads
        self.request_thread = None
        self.update_thread = None
        self.resupply_thread = None

        # format: <food_order_id: FoodOrder>
        self.food_orders = dict()

        # format <item_name: quantity>
        self.expendables = defaultdict(lambda: 0)

        self._init_mq_consumer()
        self.performance_tracker = None
        self._init_performance_tracker(self.logger)
        self.logger.info(f'Started {self.process_name}')

    def __del__(self):
        try:
            self.logger.info('Closing Flopsy Publishers Pool...')
            self.publishers.close()
        except Exception as e:
            self.logger.error(f'Exception caught while closing Flopsy Publishers Pool: {e}')

        for consumer in [self.request_consumer, self.update_consumer, self.resupply_consumer]:
            try:
                self.logger.info('Closing Flopsy Consumer...')
                consumer.close()
            except Exception as e:
                self.logger.error(f'Exception caught while closing Flopsy Consumer: {e}')

        super(Kitchen, self).__del__()

    def _init_performance_tracker(self, logger):
        # notice - we are not starting the thread. only creating an instance
        self.performance_tracker = KitchenTracker(logger)
        self.performance_tracker.start()

    def _init_mq_consumer(self):
        self.request_consumer = Consumer(QUEUE_KITCHEN_REQUEST)
        self.update_consumer = Consumer(QUEUE_KITCHEN_UPDATE)
        self.resupply_consumer = Consumer(QUEUE_KITCHEN_RESUPPLY)

    def mq_callback_update(self, message):
        new_state = FoodOrderState.from_json(message.body)
        assert isinstance(new_state, FoodOrderState)
        if new_state.order_id not in self.food_orders:
            raise KeyError(f'order_id={new_state.order_id} is beyond the horizon. '
                           f'skipping the update operation')

        self.performance_tracker.update.increment_success()
        food_order = self.food_orders[new_state.order_id]
        if new_state.state == STATE_ACCEPTED:
            # subtract the food ingredients
            for item_name, quantity in food_order.items.items():

                if self.expendables[item_name] < quantity * 2:
                    # allow for some buffer
                    supply_request = SupplyRequest(
                        item_name=item_name,
                        quantity=quantity * 5,
                        kitchen_id='this_kitchen_id',
                        hub_id='this hub id',
                        created_at=datetime.utcnow(),
                        due_at=datetime.utcnow() + timedelta(minutes=30)
                    )

                    publisher = self.publishers.get(QUEUE_PANTRY_REQUEST)
                    publisher.publish(supply_request.document)
                    publisher.release()

                    self.performance_tracker.pending.increment_success()
                    food_order.order_state.state = STATE_PENDING_RESUPPLY
                else:
                    self.expendables[item_name] -= quantity
                    self.performance_tracker.accepted.increment_success()

        if new_state.state == STATE_COOKING:
            food_order.order_state = new_state
            self.performance_tracker.cooking.increment_success()

        if new_state.state == STATE_COOKING_COMPLETE:
            # update DB record that this FoodOrder is leaving Kitchen
            # NOTE: DB portion is not implemented in the demonstrator

            # start delivery process
            food_order.order_state.state = STATE_OUT_FOR_DELIVERY
            publisher = self.publishers.get(QUEUE_DELIVERY_REQUEST)
            publisher.publish(food_order.document)
            publisher.release()

            # the Food Order is out of the Kitchen
            del self.food_orders[new_state.order_id]
            self.performance_tracker.complete.increment_success()

    def mq_callback_request(self, message):
        """ main thread MQ callback """
        try:
            food_order = FoodOrder.from_json(message.body)
            assert isinstance(food_order, FoodOrder)
            self.food_orders[food_order.order_id] = food_order

            self.request_consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.request.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.request_consumer.cancel(message.delivery_tag)
            self.performance_tracker.request.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.request_consumer.reject(message.delivery_tag)
            self.performance_tracker.request.increment_failure()

    def mq_callback_resupply(self, message):
        """ Resupply MQ callback """
        try:
            resupply = ResupplyUpdate.from_json(message.body)
            assert isinstance(resupply, ResupplyUpdate)
            self.expendables[resupply.item_name] += resupply.quantity

            # TODO: find Food Orders pending for this resupply

            self.resupply_consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.resupply.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.resupply_consumer.cancel(message.delivery_tag)
            self.performance_tracker.resupply.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.resupply_consumer.reject(message.delivery_tag)
            self.performance_tracker.resupply.increment_failure()

    def run_mq_listener(self, consumer, mq_callback):
        def runner():
            try:
                consumer.register(mq_callback)
                consumer.wait(self.mq_timeout_seconds)
            except socket.timeout as e:
                self.logger.warning(f'Queue {consumer.queue} is likely empty. Worker exits due to: {e}')
            except (AMQPError, IOError) as e:
                self.logger.error(f'AMQPError: {e}')
            finally:
                self.__del__()
                self.logger.info('Exiting main thread. All auxiliary threads stopped.')

        return runner

    def start(self, *_):
        self.request_thread = Thread(target=self.run_mq_listener(self.request_consumer, self.mq_callback_request))
        self.request_thread.start()

        self.update_thread = Thread(target=self.run_mq_listener(self.update_consumer, self.mq_callback_update))
        self.update_thread.start()

        self.resupply_thread = Thread(target=self.run_mq_listener(self.resupply_consumer, self.mq_callback_resupply))
        self.resupply_thread.start()

        from rest.kitchen import app
        app.config['kitchen'] = self
        self.flask_app = Thread(target=app.run).start()


if __name__ == '__main__':
    from constants import PROCESS_KITCHEN

    worker = Kitchen(PROCESS_KITCHEN)
    worker.start()
