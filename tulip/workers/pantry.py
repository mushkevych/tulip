__author__ = 'Bohdan Mushkevych'

import socket
from collections import defaultdict
from datetime import datetime
from threading import Thread

from amqp import AMQPError
from synergy.mq.flopsy import PublishersPool, Consumer
from synergy.system.synergy_process import SynergyProcess

import settings
from constants import QUEUE_KITCHEN_RESUPPLY, QUEUE_WAREHOUSE_REQUEST, QUEUE_PANTRY_RESUPPLY, QUEUE_PANTRY_REQUEST
from tulip.model.resupply_update import ResupplyUpdate
from tulip.model.supply_request import SupplyRequest
from tulip.system.performance_tracker import ResupplyTracker


class Pantry(SynergyProcess):
    """ Module receives scraped content from the MQ and dumps it into S3 filesystem """

    def __init__(self, process_name):
        super(Pantry, self).__init__(process_name)
        self.publishers = PublishersPool(self.logger)
        self.mq_timeout_seconds = settings.settings.get('mq_timeout_sec')

        # MQ consumers
        self.request_consumer = None
        self.update_consumer = None

        # MQ threads
        self.request_thread = None
        self.update_thread = None

        # format <name: quantity>
        self.expendables = defaultdict(lambda: 0)

        self._init_mq_consumer()
        self.performance_tracker = None
        self._init_performance_tracker(self.logger)
        self.logger.info(f'Started {self.process_name}')

    def __del__(self):
        try:
            self.logger.info('Closing Flopsy Publishers Pool...')
            self.publishers.close()
        except Exception as e:
            self.logger.error(f'Exception caught while closing Flopsy Publishers Pool: {e}')

        for consumer in [self.request_consumer, self.update_consumer]:
            try:
                self.logger.info('Closing Flopsy Consumer...')
                consumer.close()
            except Exception as e:
                self.logger.error(f'Exception caught while closing Flopsy Consumer: {e}')

        super(Pantry, self).__del__()

    def _init_performance_tracker(self, logger):
        # notice - we are not starting the thread. only creating an instance
        self.performance_tracker = ResupplyTracker(logger)
        self.performance_tracker.start()

    def _init_mq_consumer(self):
        self.request_consumer = Consumer(QUEUE_PANTRY_REQUEST)
        self.update_consumer = Consumer(QUEUE_PANTRY_RESUPPLY)

    def mq_callback_update(self, message):
        """ Resupply MQ callback """
        try:
            resupply = ResupplyUpdate.from_json(message.body)
            assert isinstance(resupply, ResupplyUpdate)
            self.expendables[resupply.item_name] += resupply.quantity

            # TODO: find Kitchen Requests pending for this resupply

            self.update_consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.update.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.update_consumer.cancel(message.delivery_tag)
            self.performance_tracker.update.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.update_consumer.reject(message.delivery_tag)
            self.performance_tracker.update.increment_failure()

    def mq_callback_request(self, message):
        try:
            supply_request = SupplyRequest.from_json(message.body)
            assert isinstance(supply_request, SupplyRequest)

            # TODO: add logic that would queue requests which could be answered once the warehouse resupplies

            # scenario A: we have the requested supply
            if self.expendables[supply_request.item_name] >= supply_request.quantity:
                # step 1: ensure physical resupply
                # NOTE: not implemented in the demonstrator

                # step 2: decrease stock numbers
                self.expendables[supply_request.item_name] -= supply_request.quantity

                # step 3: notify about resupply
                resupply_update = ResupplyUpdate(
                    item_name=supply_request.item_name,
                    quantity=supply_request.quantity,
                    kitchen_id=supply_request.kitchen_id,
                    hub_id=supply_request.hub_id,
                    created_at=datetime.utcnow()
                )
                publisher = self.publishers.get(QUEUE_KITCHEN_RESUPPLY)
                publisher.publish(resupply_update.document)
                publisher.release()
                self.performance_tracker.update.increment_success()

            # scenario B: we need to request the supply from the warehouse
            if self.expendables[supply_request.item_name] < supply_request.quantity * 5:
                supply_request.quantity *= 5
                publisher = self.publishers.get(QUEUE_WAREHOUSE_REQUEST)
                publisher.publish(supply_request.document)
                publisher.release()
                self.performance_tracker.delegate.increment_success()

            self.request_consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.request.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.request_consumer.cancel(message.delivery_tag)
            self.performance_tracker.request.increment_failure()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.request_consumer.reject(message.delivery_tag)
            self.performance_tracker.request.increment_failure()

    def run_mq_listener(self, consumer, mq_callback):
        def runner():
            try:
                consumer.register(mq_callback)
                consumer.wait(self.mq_timeout_seconds)
            except socket.timeout as e:
                self.logger.warning(f'Queue {consumer.queue} is likely empty. Worker exits due to: {e}')
            except (AMQPError, IOError) as e:
                self.logger.error(f'AMQPError: {e}')
            finally:
                self.__del__()
                self.logger.info('Exiting main thread. All auxiliary threads stopped.')

        return runner

    def start(self, *_):
        self.request_thread = Thread(target=self.run_mq_listener(self.request_consumer, self.mq_callback_request))
        self.request_thread.start()

        self.update_thread = Thread(target=self.run_mq_listener(self.update_consumer, self.mq_callback_update))
        self.update_thread.start()


if __name__ == '__main__':
    from constants import PROCESS_PANTRY

    worker = Pantry(PROCESS_PANTRY)
    worker.start()
