__author__ = 'Bohdan Mushkevych'

from datetime import datetime

from synergy.mq.flopsy import PublishersPool, Consumer
from synergy.workers.abstract_mq_worker import AbstractMqWorker

from constants import QUEUE_PANTRY_RESUPPLY, QUEUE_WAREHOUSE_REQUEST
from tulip.model.resupply_update import ResupplyUpdate
from tulip.model.supply_request import SupplyRequest
from tulip.system.performance_tracker import ResupplyTracker


class Warehouse(AbstractMqWorker):
    """ Module receives re-stocking requests from Kitchen Hubs """

    def __init__(self, process_name):
        super(Warehouse, self).__init__(process_name)
        self.publishers = PublishersPool(self.logger)

    def __del__(self):
        try:
            self.logger.info('Closing Flopsy Publishers Pool...')
            self.publishers.close()
        except Exception as e:
            self.logger.error(f'Exception caught while closing Flopsy Publishers Pool: {e}')
        super(Warehouse, self).__del__()

    def _init_performance_tracker(self, logger):
        # notice - we are not starting the thread. only creating an instance
        self.performance_tracker = ResupplyTracker(logger)
        self.performance_tracker.start()

    def _init_mq_consumer(self):
        self.consumer = Consumer(QUEUE_WAREHOUSE_REQUEST)

    def _mq_callback(self, message):
        try:
            supply_request = SupplyRequest.from_json(message.body)
            assert isinstance(supply_request, SupplyRequest)

            # NOTE: we assume that warehouse can satisfy any supply requests

            # step 1: ensure physical resupply
            # NOTE: not implemented in the demonstrator

            # step 2: notify about resupply
            resupply_update = ResupplyUpdate(
                item_name=supply_request.item_name,
                quantity=supply_request.quantity,
                kitchen_id=supply_request.kitchen_id,
                hub_id=supply_request.hub_id,
                created_at=datetime.utcnow()
            )
            publisher = self.publishers.get(QUEUE_PANTRY_RESUPPLY)
            publisher.publish(resupply_update.document)
            publisher.release()
            self.performance_tracker.update.increment_success()

            self.consumer.acknowledge(message.delivery_tag)
            self.performance_tracker.request.increment_success()
        except (KeyError, IndexError) as e:
            self.logger.error(f'Error is considered Unrecoverable: {e}\nCancelled message: {message.body}')
            self.consumer.cancel(message.delivery_tag)
            self.performance_tracker.request.increment_success()
        except Exception as e:
            self.logger.error(f'Error is considered Recoverable: {e}\nRe-queueing message: {message.body}')
            self.consumer.reject(message.delivery_tag)
            self.performance_tracker.request.increment_success()


if __name__ == '__main__':
    from constants import PROCESS_WAREHOUSE

    worker = Warehouse(PROCESS_WAREHOUSE)
    worker.start()
