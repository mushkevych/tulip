__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField

from tulip.model.model_constants import *


class FoodOrderState(BaseDocument):
    order_id = ObjectIdField()
    state = StringField(choices=[STATE_CREATED, STATE_ACCEPTED, STATE_PENDING_RESUPPLY,
                                 STATE_COOKING, STATE_COOKING_COMPLETE, STATE_OUT_FOR_DELIVERY,
                                 STATE_IN_TRANSIT, STATE_DELIVERED, STATE_CANCELED])
    last_updated = StringField()
