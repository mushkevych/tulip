__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField


class Kitchen(BaseDocument):
    kitchen_id = ObjectIdField()
    kitchen_name = StringField()
    kitchen_style = StringField()
