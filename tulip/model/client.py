__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField, DecimalField


class Client(BaseDocument):
    client_id = ObjectIdField()
    first_name = StringField()
    last_name = StringField()
    rating = DecimalField(precision=2)
