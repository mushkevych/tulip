__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField, NestedDocumentField

from tulip.model.address import Address


class Hub(BaseDocument):
    hub_id = ObjectIdField()
    hub_name = StringField()
    address = NestedDocumentField(Address)
