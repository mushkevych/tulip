__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import ObjectIdField, DecimalField, DictField, DateTimeField, NestedDocumentField

from tulip.model.address import Address
from tulip.model.client import Client
from tulip.model.hub import Hub
from tulip.model.kitchen import Kitchen
from tulip.model.food_order_state import FoodOrderState


class FoodOrder(BaseDocument):
    order_id = ObjectIdField()
    client = NestedDocumentField(Client)
    delivery_address = NestedDocumentField(Address)
    kitchen = NestedDocumentField(Kitchen)
    hub = NestedDocumentField(Hub)
    order_state = NestedDocumentField(FoodOrderState)

    created_at = DateTimeField()
    due_at = DateTimeField()

    # ordered items in format: <item_name: quantity>
    items = DictField()
    total_value = DecimalField()
