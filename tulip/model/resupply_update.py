__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField, DateTimeField, IntegerField


class ResupplyUpdate(BaseDocument):
    """ resupply update with ingredient/expandable from Hub's Pantry to Kitchen """
    item_name = StringField()
    quantity = IntegerField()

    kitchen_id = ObjectIdField()
    hub_id = ObjectIdField()

    created_at = DateTimeField()
