__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField


class Address(BaseDocument):
    country = StringField()
    state = StringField()
    city = StringField()
    street = StringField()
    details = StringField()
