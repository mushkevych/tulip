__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField, DateTimeField, IntegerField


class SupplyRequest(BaseDocument):
    """ ingredient/expandable request from Kitchen to Hub's Pantry """
    item_name = StringField()
    quantity = IntegerField()

    kitchen_id = ObjectIdField()
    hub_id = ObjectIdField()

    created_at = DateTimeField()
    due_at = DateTimeField()
