__author__ = 'Bohdan Mushkevych'

from odm.document import BaseDocument
from odm.fields import StringField, ObjectIdField, DateTimeField


class WarehouseRequest(BaseDocument):
    """ ingredient/expandable request from Hub's Pantry to Nearby Warehouse """
    item_name = StringField()
    hub_id = ObjectIdField()
    warehouse_id = ObjectIdField()
    created_at = DateTimeField()
    due_at = DateTimeField()
