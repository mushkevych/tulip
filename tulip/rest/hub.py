from tulip.model.address import Address

expose = lambda _: _


@expose('/v1/hub/{hub_id}/{kitchen_id}/date')
def capacity(hub_id, kitchen_id) -> dict:
    """ returns a dict[timeperiod][items_capacity], where
        * timeperiod is in 30-min steps YYYYMMDDQQ, QQ = [00...47] (because 24 *2 = 48)
        * items_capacity is the number of food items given kitchen can process in the given timeperiod
    """
    pass


@expose('/v1/hub/{hub_id}/{timeperiod}/date')
def delivery_time(hub_id, timeperiod, address:Address) -> int:
    """ returns time in minutes the delivery is expected to take
        from the given Kitchen Hub to the specified delivery_address in the range of the specified timeperiod
    """
    pass
