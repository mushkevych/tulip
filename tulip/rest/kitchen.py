import threading

from flask import Flask

# expose = lambda _: _
#
#
# @expose('/v1/menu/{hub_id}/{kitchen_id}')
def menu(hub_id, kitchen_id) -> dict:
    """ returns a dict[item_name][item_capacity], where
        * item_name the name of a menu entry (for instance: Salmon Onigiri)
        * items_capacity estimated highest number of the item Kitchen can produce without re-supply from Warehouse
    """
    return None


app = Flask(__name__)


@app.route("/orders")
def orders():
    kitchen = app.config['kitchen']
    return kitchen.food_orders


if __name__ == "__main__":
    threading.Thread(target=app.run).start()
