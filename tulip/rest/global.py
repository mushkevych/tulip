from typing import List

from tulip.model.address import Address
from tulip.model.hub import Hub

expose = lambda _: _


@expose('/v1/hubs/near')
def hubs_near(address:Address) -> List[Hub]:
    """ returns a list of Hub records """
    pass
