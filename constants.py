__author__ = 'Bohdan Mushkevych'


# List of Processes
PROCESS_LAUNCH_PY = 'LaunchPy'
PROCESS_KITCHEN = 'Kitchen'
PROCESS_PANTRY = 'Pantry'
PROCESS_RECEPTION = 'Reception'
PROCESS_DELIVERY = 'Delivery'
PROCESS_WAREHOUSE = 'Warehouse'
PROCESS_ORDER_GENERATOR = 'KitchenUpdateGenerator'

# Process tokens. There should be one token per one Timetable tree or stand-alone process
TOKEN_LAUNCH_PY = 'launch_py'
TOKEN_KITCHEN = 'kitchen'
TOKEN_PANTRY = 'pantry'
TOKEN_RECEPTION = 'reception'
TOKEN_DELIVERY = 'delivery'
TOKEN_WAREHOUSE = 'warehouse'
TOKEN_ORDER_GENERATOR = 'generator'

QUEUE_RECEPTION_REQUEST = 'q_reception'
QUEUE_KITCHEN_REQUEST = 'q_kitchen_request'
QUEUE_KITCHEN_RESUPPLY = 'q_kitchen_resupply'
QUEUE_KITCHEN_UPDATE = 'q_kitchen_update'
QUEUE_DELIVERY_REQUEST = 'q_delivery_request'
QUEUE_DELIVERY_UPDATE = 'q_delivery_update'
QUEUE_PANTRY_REQUEST = 'q_pantry_request'
QUEUE_PANTRY_RESUPPLY = 'q_pantry_resupply'
QUEUE_WAREHOUSE_REQUEST = 'q_warehouse_request'

ROUTING_IRRELEVANT = 'routing_irrelevant'
ROUTING_DEMO = 'routing_demonstrator'
EXCHANGE_MAIN = 'ex_main'
