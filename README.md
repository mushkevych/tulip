Tulip
==========

Tulip project is an event-driven architecture demonstrator for Kitchen Hub, where:  
- Kitchens are grouped into Hubs, sharing same location (for instance in former Toys'r'us stores)  
- Rabbit MQ is used as a message bus and provides the only persistent tier  
- Queue names are consistent among the hubs (i.e. `q_warehouse_request` or `q_kitchen_request`)  while the routing is performed via `ROUTING_KEY` and load balancing - via `exchange`

Details on AMQP concepts are available at this [link](https://www.rabbitmq.com/tutorials/amqp-concepts.html)

architecture
---------

![Architecture](./draw.io/Tulip.png)

how-to
---------
1. start the rabbit-mq  
    ```docker run -d --hostname syn-rabbitmq --name syn-rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management```  
    or  
    ```docker start syn-rabbitmq```  

1. create log folders  
    ```mkdir -p /tmp/logs/tulip/; chmod 777 /tmp/logs/tulip```

1. install Virtual Environment 
    ```
    ./launch.py install
    ```

1. start daemons
    ```
    ./launch.py start Reception
    ./launch.py start Kitchen
    ./launch.py start Pantry
    ./launch.py start Warehouse
    ./launch.py start Delivery
    ./launch.py start FoodOrderGenerator
    ```


metafile:
---------

    /launch.py            main executing file  
    /process_starter.py   utility to start worker in daemon mode  
    /settings.py          configuration management  
    /scripts/             folder contains shell scripts  
    /system/              folder contains useful system-level modules  
    /tests/               folder contains unit test  
    /draw.io/             folder contains architecture diagram   


license:
---------

Apache 2.0 License:
http://www.apache.org/licenses/LICENSE-2.0
