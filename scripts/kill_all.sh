#!/bin/bash

# script kills all processes with Tulip in its name

ps aux | grep Tulip | grep -v grep | awk '{print$2}' | xargs kill
