__author__ = 'Bohdan Mushkevych'

from synergy.db.model.daemon_process_entry import daemon_context_entry
from synergy.db.model.queue_context_entry import queue_context_entry
from synergy.scheduler.scheduler_constants import *

from constants import *

mq_queue_context = {
    # contains "raw" food orders from customers
    QUEUE_RECEPTION_REQUEST: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_RECEPTION_REQUEST),

    # contains food order for a given kitchen
    QUEUE_KITCHEN_REQUEST: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_KITCHEN_REQUEST),

    # carries resupply updates from Pantry to a given Kitchen
    QUEUE_KITCHEN_RESUPPLY: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_KITCHEN_RESUPPLY),

    # carries FoodOrder state updates
    QUEUE_KITCHEN_UPDATE: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_KITCHEN_UPDATE),

    # contains supply requests to a warehouse from a kitchen hub from a warehouse
    QUEUE_WAREHOUSE_REQUEST: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_WAREHOUSE_REQUEST),

    # contains requests to resupply kitchen from a local pantry
    QUEUE_PANTRY_REQUEST: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_PANTRY_REQUEST),

    # contains supply events from warehouse to a local pantry
    QUEUE_PANTRY_RESUPPLY: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_PANTRY_RESUPPLY),

    # contains cooked food delivery requests
    QUEUE_DELIVERY_REQUEST: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_DELIVERY_REQUEST),

    # contains delivery update events
    QUEUE_DELIVERY_UPDATE: queue_context_entry(
        exchange=EXCHANGE_MAIN,
        queue_name=QUEUE_DELIVERY_UPDATE),
}

process_context = {
    PROCESS_LAUNCH_PY: daemon_context_entry(
        process_name=PROCESS_LAUNCH_PY,
        classname='',
        token=TOKEN_LAUNCH_PY,
        routing=ROUTING_IRRELEVANT,
        exchange=EXCHANGE_UTILS),

    PROCESS_RECEPTION: daemon_context_entry(
        process_name=PROCESS_RECEPTION,
        classname='tulip.workers.reception.Reception.start',
        token=TOKEN_RECEPTION),

    PROCESS_KITCHEN: daemon_context_entry(
        process_name=PROCESS_KITCHEN,
        classname='tulip.workers.kitchen.Kitchen.start',
        token=TOKEN_KITCHEN),

    PROCESS_PANTRY: daemon_context_entry(
        process_name=PROCESS_PANTRY,
        classname='tulip.workers.pantry.Pantry.start',
        token=TOKEN_PANTRY),

    PROCESS_WAREHOUSE: daemon_context_entry(
        process_name=PROCESS_WAREHOUSE,
        classname='tulip.workers.warehouse.Warehouse.start',
        token=TOKEN_WAREHOUSE,
        exchange=EXCHANGE_MAIN),

    PROCESS_DELIVERY: daemon_context_entry(
        process_name=PROCESS_DELIVERY,
        classname='tulip.workers.delivery.Delivery.start',
        token=TOKEN_DELIVERY),

    PROCESS_ORDER_GENERATOR: daemon_context_entry(
        process_name=PROCESS_ORDER_GENERATOR,
        classname='streamgen.food_order_generator.KitchenUpdateGenerator.start',
        token=TOKEN_ORDER_GENERATOR),
}

timetable_context = {
}
