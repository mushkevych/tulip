__author__ = 'Bohdan Mushkevych'

import random
import time
from threading import Thread

from amqp import AMQPError
from synergy.mq.flopsy import Publisher
from synergy.system.performance_tracker import SimpleTracker
from synergy.system.synergy_process import SynergyProcess

from constants import QUEUE_KITCHEN_UPDATE
from tulip.model.food_order import FoodOrder
from tulip.model.model_constants import *

SLEEP_TIME = 0.5
TICK_INTERVAL = 10


class KitchenUpdateGenerator(SynergyProcess):
    """ illustration suite worker:
        - emulates cooks activity in the kitchen """

    def __init__(self, process_name):
        super(KitchenUpdateGenerator, self).__init__(process_name)
        self.main_thread = None
        self.publisher = Publisher(QUEUE_KITCHEN_UPDATE)
        self.performance_tracker = SimpleTracker(self.logger)
        self.thread_is_running = True

        self.logger.info(f'Started {self.process_name}')

    def __del__(self):
        self.publisher.close()
        self.performance_tracker.cancel()
        super(KitchenUpdateGenerator, self).__del__()
        self.logger.info('Exiting main thread. All auxiliary threads stopped.')

    def _run_stream_generation(self):
        rate = 1 / SLEEP_TIME
        self.logger.info(
            f'Stream Generator: ON. Expected rate: {rate}/s, {rate * 60}/m, {rate * 3600}/h, {rate * 86400}/d')
        self.performance_tracker.start()
        random.seed('RANDOM_SEED_OBJECT')

        while self.thread_is_running:
            try:
                food_order = FoodOrder()
                food_order.order_id = f'fo::{random.randint(0, 10000)}'

                if food_order.order_state.state == STATE_CREATED:
                    # move this order_id into STATE_ACCEPTED

                if food_order.order_state.state == STATE_ACCEPTED:
                    # move this order_id into STATE_COOKING

                if food_order.order_state.state == STATE_COOKING:
                    # move this order_id into STATE_COOKING_COMPLETE

                if food_order.order_state.state == STATE_COOKING_COMPLETE:
                    # move this order_id into STATE_OUT_FOR_DELIVERY

                if food_order.order_state.state == STATE_OUT_FOR_DELIVERY:
                    # move this order_id into STATE_IN_TRANSIT

                if food_order.order_state.state == STATE_IN_TRANSIT:
                    # move this order_id into STATE_DELIVERED

                self.publisher.publish(food_order.document)
                self.performance_tracker.tracker.increment_success()
                time.sleep(SLEEP_TIME)
            except (AMQPError, IOError) as e:
                self.thread_is_running = False
                self.performance_tracker.cancel()
                self.logger.error(f'AMQPError: {e}')
            except Exception as e:
                self.performance_tracker.tracker.increment_failure()
                self.logger.info(f'safety fuse: {e}')

    def start(self, *_):
        self.main_thread = Thread(target=self._run_stream_generation)
        self.main_thread.start()

    def cancel(self):
        self.thread_is_running = False


if __name__ == '__main__':
    from constants import PROCESS_ORDER_GENERATOR

    generator = KitchenUpdateGenerator(PROCESS_ORDER_GENERATOR)
    generator._run_stream_generation()
    # generator.start()
