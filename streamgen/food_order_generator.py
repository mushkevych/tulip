__author__ = 'Bohdan Mushkevych'

import datetime
import decimal
import random
import time
from threading import Thread

from amqp import AMQPError
from constants import QUEUE_RECEPTION_REQUEST
from synergy.mq.flopsy import Publisher
from synergy.system.performance_tracker import SimpleTracker
from synergy.system.synergy_process import SynergyProcess

from tulip.model.address import Address
from tulip.model.client import Client
from tulip.model.hub import Hub
from tulip.model.kitchen import Kitchen
from tulip.model.model_constants import STATE_CREATED
from tulip.model.food_order import FoodOrder

SLEEP_TIME = 0.5
TICK_INTERVAL = 10


class FoodOrderGenerator(SynergyProcess):
    """ illustration suite worker:
        - emulates user activity on the web site """

    def __init__(self, process_name):
        super(FoodOrderGenerator, self).__init__(process_name)
        self.main_thread = None
        self.publisher = Publisher(QUEUE_RECEPTION_REQUEST)
        self.performance_tracker = SimpleTracker(self.logger)
        self.thread_is_running = True

        self.logger.info(f'Started {self.process_name}')

    def __del__(self):
        self.publisher.close()
        self.performance_tracker.cancel()
        super(FoodOrderGenerator, self).__del__()
        self.logger.info('Exiting main thread. All auxiliary threads stopped.')

    def _run_stream_generation(self):
        rate = 1 / SLEEP_TIME
        self.logger.info(
            f'Stream Generator: ON. Expected rate: {rate}/s, {rate * 60}/m, {rate * 3600}/h, {rate * 86400}/d')
        self.performance_tracker.start()
        random.seed('RANDOM_SEED_OBJECT')

        while self.thread_is_running:
            try:
                food_order = FoodOrder()
                food_order.order_id = f'fo::{random.randint(0, 10000)}'
                food_order.client = Client(
                    client_id=f'c_{random.randint(0, 10000)}',
                    first_name=random.choice(['Jason', 'Oly', 'Mary', 'Zak', 'Lila']),
                    last_name=random.choice(['Kit', 'Sim', 'Lim', 'Dim', 'Joee']),
                    rating=decimal.Decimal(random.random() * 10)
                )
                food_order.delivery_address = Address(
                    country=random.choice(['Mexico', 'Canada', 'USA']),
                    state=random.choice(['OR', 'WA', 'AL', 'BC', 'ID', 'OH']),
                    city=random.choice(['Portland', 'Vancouver', 'Seattle', 'Boise', 'Mexico']),
                    street=random.choice(['100', '135', 'First', 'Second', 'Third']),
                    details=random.choice(['dont ring', 'code 9876', 'left door', 'app 156', '']),
                )
                food_order.kitchen = Kitchen(
                    kitchen_id=f"k_{random.choice(['Sushi', 'Chinese', 'Burrito', 'Burger', 'Pizza'])}",
                    kitchen_name=f'name_{random.randint(0, 25)}',
                    kitchen_style=f"{random.choice(['Sushi', 'Chinese', 'Mexican', 'Burger', 'Pizza'])}"
                )
                food_order.hub = Hub(
                    hub_id=f'h_{random.randint(0, 10)}',
                    hub_name=random.choice(['Portland::South', 'Seattle::South', 'Seattle::North', 'Calgary::South']),
                    address=Address(
                        country=random.choice(['Canada', 'USA']),
                        state=random.choice(['OR', 'WA', 'AL']),
                        city=random.choice(['Portland', 'Seattle', 'Calgary']),
                        street=random.choice(['100', '135', 'First', 'Second', 'Third'])
                    )
                )
                food_order.order_state.state = STATE_CREATED

                food_order.created_at = datetime.datetime.utcnow()
                food_order.due_at = datetime.datetime.utcnow() + datetime.timedelta(hours=1)

                # ordered items in format: <item_name: quantity>
                if food_order.kitchen.kitchen_style == 'Sushi':
                    items = {
                        'california roll': random.randint(1, 2),
                        'salmon onigiri': random.randint(0, 4),
                        'tuna onigiri': random.randint(0, 4),
                        'teriyaki unagi': random.randint(0, 1),
                        'gyoza': random.randint(0, 2),
                        'miso soup': random.randint(0, 3),
                        'packaging': random.randint(1, 3),
                        'wasabi': random.randint(1, 2),
                        'hashi': random.randint(1, 3),
                        'ginger': random.randint(1, 2),
                    }
                elif food_order.kitchen.kitchen_style == 'Chinese':
                    items = {
                        'sweet and sour pork': random.randint(0, 2),
                        'sweet and sour chicken': random.randint(0, 2),
                        'fried rice': random.randint(1, 2),
                        'chicken chow': random.randint(0, 2),
                        'pork chow': random.randint(0, 2),
                        'wonton soup': random.randint(0, 2),
                        'packaging': random.randint(1, 3),
                        'hashi': random.randint(1, 3),
                        'ginger': random.randint(1, 2),
                        'fortune cookies': random.randint(2, 3),
                    }
                elif food_order.kitchen.kitchen_style == 'Mexican':
                    items = {
                        'burito': random.randint(0, 1),
                        'traditional': random.randint(0, 2),
                        'spicy': random.randint(0, 1),
                        'packaging': random.randint(1, 3),
                    }
                elif food_order.kitchen.kitchen_style == 'Burger':
                    items = {
                        'bbq': random.randint(0, 1),
                        'vegetarian': random.randint(0, 2),
                        'traditional': random.randint(0, 1),
                        'roasted pork': random.randint(0, 1),
                        'packaging': random.randint(1, 3),
                    }
                elif food_order.kitchen.kitchen_style == 'Pizza':
                    items = {
                        'cheese': random.randint(0, 1),
                        'paperoni': random.randint(0, 1),
                        'canadian beacon': random.randint(0, 1),
                        'bbq': random.randint(0, 1),
                        'packaging': random.randint(1, 3),
                    }
                else:
                    items = {}
                    self.logger.error(f'Unknown kitchen style {food_order.kitchen.kitchen_style}')

                food_order.items = items
                food_order.total_value = random.randint(30, 75)

                self.publisher.publish(food_order.document)
                self.performance_tracker.tracker.increment_success()
                time.sleep(SLEEP_TIME)
            except (AMQPError, IOError) as e:
                self.thread_is_running = False
                self.performance_tracker.cancel()
                self.logger.error(f'AMQPError: {e}')
            except Exception as e:
                self.performance_tracker.tracker.increment_failure()
                self.logger.info(f'safety fuse: {e}')

    def start(self, *_):
        self.main_thread = Thread(target=self._run_stream_generation)
        self.main_thread.start()

    def cancel(self):
        self.thread_is_running = False


if __name__ == '__main__':
    from constants import PROCESS_ORDER_GENERATOR

    generator = FoodOrderGenerator(PROCESS_ORDER_GENERATOR)
    generator._run_stream_generation()
    # generator.start()
